export class Message {
  constructor(
    public password: string,
    public message: string
  ) {}
}

export class Decode {
  constructor(
    public decode: string
  ) {}
}

export class Encode {
  constructor(
    public encode: string
  ) {}
}

import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Decode, Encode, Message } from '../../models/message.model';
import { MessageService } from '../../services/message.service';

@Component({
  selector: 'app-encode-form',
  templateUrl: './encode-form.component.html',
  styleUrls: ['./encode-form.component.sass']
})
export class EncodeFormComponent {
  @ViewChild('form') form!: NgForm;

  constructor(private messageService: MessageService) { }

  onClickEncodeBtn() {
    const message: Message = {
      password: this.form.value.passwordValue,
      message: this.form.value.encodeValue
    }
    this.messageService.postEncode(message).subscribe( encode => {
      this.setFormValue({
        decodeValue: (<Encode>encode).encode,
        encodeValue: '',
        passwordValue: this.form.value.passwordValue,
      });
    })
  }

  onClickDecodeBtn() {
    const message: Message = {
      password: this.form.value.passwordValue,
      message: this.form.value.decodeValue
    }
    this.messageService.postDecode(message).subscribe( decode => {
      this.setFormValue({
        encodeValue: (<Decode>decode).decode,
        decodeValue: '',
        passwordValue: this.form.value.passwordValue,
      });
    })
  }

  setFormValue(value: { [key: string]: any }) {
    setTimeout(() => {
      this.form.form.setValue(value);
    });
  }
}

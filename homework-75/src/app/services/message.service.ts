import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Message } from '../models/message.model';

@Injectable({
  providedIn: 'root'
})

export class MessageService {
  constructor(private http: HttpClient) {}

  postEncode (message: Message) {
    return this.http.post(`http://localhost:8000/encode`, message);
  }

  postDecode (message: Message) {
    return this.http.post('http://localhost:8000/decode', message);
  }
}
